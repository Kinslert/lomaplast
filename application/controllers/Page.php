<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Page extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}
	public function home(){
		$this->load->view('home');
		$this->load->view('Template/footer');
	}

	public function articulos(){
		$vista_data = array('title' => 'Loma Hermosa Plast'.' '.'-'.' '.$this->uri->segment(1));
		
		$this->load->view('Template/header', $vista_data);
		$this->load->view('Template/sidebar');
		$this->load->view('articulos');
		$this->load->view('Template/footer');
	}
	public function error404(){
		$vista_data = array('title' => 'Loma Hermosa Plast'.' '.'-'.' '.$this->uri->segment(1));
		$this->load->view('Template/header', $vista_data);
		$this->load->view('error404');
		$this->load->view('Template/footer');

	}
	public function Index(){
		$vista_data = array('title' => 'Loma Hermosa Plast'.' '.'-'.' '.$this->uri->segment(1));
		$this->load->view('Template/header', $vista_data);
		$this->load->view('index');
		$this->load->view('Template/footer');
	}
	public function categorias(){
		$vista_data = array('title' => 'Loma Hermosa Plast'.' '.'-'.' '.$this->uri->segment(1));
		$this->load->view('Template/header', $vista_data);
		$this->load->view('categorias');
		$this->load->view('Template/footer');
	}
	public function categoria(){
		$vista_data = array('title' => 'Loma Hermosa Plast'.' '.'-'.' '.$this->uri->segment(1));
		$this->load->view('Template/header', $vista_data);
		$this->load->view('Template/sidebar');
		$this->load->view('categoria');
		$this->load->view('Template/footer');
	}
	public function ford(){
		$vista_data = array('title' => 'Loma Hermosa Plast'.' '.'-'.' '.$this->uri->segment(1));
		$this->load->view('Template/header', $vista_data);
		$this->load->view('ford');
		$this->load->view('Template/footer');
	}
		public function fiat(){
		$vista_data = array('title' => 'Loma Hermosa Plast'.' '.'-'.' '.$this->uri->segment(1));
		$this->load->view('Template/header', $vista_data);
		$this->load->view('fiat');
		$this->load->view('Template/footer');
	}
		public function chevrolet(){
		$vista_data = array('title' => 'Loma Hermosa Plast'.' '.'-'.' '.$this->uri->segment(1));
		$this->load->view('Template/header', $vista_data);
		$this->load->view('chevrolet');
		$this->load->view('Template/footer');
	}
		public function toyota(){
		$vista_data = array('title' => 'Loma Hermosa Plast'.' '.'-'.' '.$this->uri->segment(1));
		$this->load->view('Template/header', $vista_data);
		$this->load->view('toyota');
		$this->load->view('Template/footer');
	}
		public function renault(){
		$vista_data = array('title' => 'Loma Hermosa Plast'.' '.'-'.' '.$this->uri->segment(1));
		$this->load->view('Template/header', $vista_data);
		$this->load->view('renault');
		$this->load->view('Template/footer');
	}
		public function volkswagen(){
		$vista_data = array('title' => 'Loma Hermosa Plast'.' '.'-'.' '.$this->uri->segment(1));
		$this->load->view('Template/header', $vista_data);
		$this->load->view('volkswagen');
		$this->load->view('Template/footer');
	}
	public function catalogo(){
		$vista_data = array('title' => 'Loma Hermosa Plast'.' '.'-'.' '.$this->uri->segment(1));
		$this->load->view('Template/header', $vista_data);
		$this->load->view('catalogo');
		$this->load->view('Template/footer');
	}
	public function notice(){
		$vista_data = array('title' => 'Loma Hermosa Plast'.' '.'-'.' '.$this->uri->segment(1));
		$this->load->view('Template/header', $vista_data);
		$this->load->view('notice');
		$this->load->view('Template/footer');
	}
	public function about(){
		$vista_data = array('title' => 'Loma Hermosa Plast'.' '.'-'.' '.$this->uri->segment(1));
		$this->load->view('Template/header', $vista_data);
		$this->load->view('about');
		$this->load->view('Template/footer');
	}
	public function contact(){
		$vista_data = array('title' => 'Loma Hermosa Plast'.' '.'-'.' '.$this->uri->segment(1));
		$this->load->view('Template/header', $vista_data);
		$this->load->view('contact');
		$this->load->view('Template/footer');
	}

	public function send_form(){
		/*
		|--------------------------------------------------------------------------
		| email settings
		|--------------------------------------------------------------------------
		*/
		$name = $this->input->post('name');
		$apellido = $this->input->post('apellido');
		$razon = $this->input->post('razon');
		$direccion = $this->input->post('direccion');
		$ciudad = $this->input->post('ciudad');
		$zp = $this->input->post('zp');
		$pais = $this->input->post('pais');
		$email = $this->input->post('email');
		$comment = $this->input->post('comment');
		/*
		|--------------------------------------------------------------------------
		| email configs
		|--------------------------------------------------------------------------
		*/
		$this->email->initialize($config);
		/*
		|--------------------------------------------------------------------------
		| send mail
		|--------------------------------------------------------------------------
		*/
		$this->email->from($email, $email);
		$this->email->to('lomaplast@desarrolladorespweb.com.ve');
		/* 
		$this->email->cc('another@another-example.com'); 
		$this->email->bcc('them@their-example.com'); 
		*/
		$this->email->subject('Contacto - cliente');
		$this->email->message('<b>Datos del solicitante:<b>'
			.'<b><h3>Nombre:'.$name.'</b>'.'</h3>'
			.'<b><h3>Apellido:'.$apellido.'</b>'.'</h3>'
			.'<b><h3>Razón Social:'.$razon.'</b>'.'</h3>'
			.'<b><h3>Dirección:'.$direccion.'</b>'.'</h3>'
			.'<b><h3>Ciudad:'.$ciudad.'</b>'.'</h3>'
			.'<b><h3>Codigo postal:'.$zp.'</b>'.'</h3>'
			.'<b><h3>Pais:'.$pais.'</b>'.'</h3>'.
			'<b><h3>email:'.$email.'</b>'.'</h3>'.
			'<b><h3>Comentario:</h3></b>'.
			$comment);	
		$this->email->send();
	}

}

 ?>