<!-- Content
================================================== -->
<div class="twelve columns">


</div>

<!-- Products -->
<div class="twelve columns products">

<!-- Product #1 -->
<div class="four shop columns">
    <figure class="product">
        <div class="">
            <a href="<?php echo base_url(); ?>articulos">
                <img style="height: 150px;" alt="" src="images/001.jpg"/>

            </a>
        </div>

        <a href="<?php echo base_url(); ?>articulos">
            <section>

                <h5>CHEVROLET CLASSIC 10</h5>

            </section>
        </a>
    </figure>
</div>


<!-- Product #2 -->
<div class="four shop columns">
    <figure class="product">
        <div class="">
            <a href="<?php echo base_url(); ?>articulos">
                <img style="height: 150px;" alt="" src="images/001.jpg"/>

            </a>
        </div>

        <a href="<?php echo base_url(); ?>articulos">
            <section>

                <h5>    CHEVROLET CORSA 00/08
                </h5>

            </section>
        </a>
    </figure>
</div>


<!-- Product #3 -->
<div class="four shop columns">
    <figure class="product">
        <div class="">
            <a href="<?php echo base_url(); ?>articulos">
                <img style="height: 150px;" alt="" src="images/001.jpg"/>

            </a>
        </div>

        <a href="<?php echo base_url(); ?>articulos">
            <section>

                <h5>CHEVROLET CORSA 08/10</h5>

            </section>
        </a>
    </figure>
</div>

<div class="clearfix"></div>

<!-- Pagination -->
<div class="pagination-container">
    <nav class="pagination">
        <ul>
            <li><a href="#" class="current-page">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
        </ul>
    </nav>

    <nav class="pagination-next-prev">
        <ul>
            <li><a href="#" class="prev"></a></li>
            <li><a href="#" class="next"></a></li>
        </ul>
    </nav>
</div>

</div>

</div>