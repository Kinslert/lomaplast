
<!-- Content
================================================== -->

<!-- Container -->
<div class="container">

	<div class="sixteen columns">
		<section id="not-found">
			<h2>404 <i class="fa fa-question-circle"></i></h2>
			<p>Lo sentimos, pero la página que estás buscando no existe.</p>
		</section>
	</div>

</div>
<!-- Container / End -->

<div class="margin-top-50"></div>
