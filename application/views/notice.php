


<!-- Content
================================================== -->

<!-- Container -->
<div class="container">

<!-- Filters -->
<div class="sixteen columns">
	<div id="filters" class="filters-dropdown">
		<ul class="option-set">
			<li class="active"><a href="#filter" class="selected" data-filter="*">All</a></li>
			<li><a href="#filter" data-filter=".accessories">Accessories</a></li>
			<li><a href="#filter" data-filter=".clothing">Clothing</a></li>
			<li><a href="#filter" data-filter=".technology">Technology</a></li>
			<li><a href="#filter" data-filter=".food">Food</a></li>
			<li><a href="#filter" data-filter=".other">Other</a></li>
		</ul>
	</div>
</div>
<div class="clearfix"></div>

<ul id="og-grid" class="og-grid">

	<!-- Project -->
	<li class="clothing others clickable">

		<a href="#" class="grid-item-image">
			<div class="dynamic-portfolio-holder">
				<img src="<?php echo base_url() ?>images/portfolio/portfolio-01.jpg" alt=""/>
				<div class="hover-cover"></div>
				<div class="hover-icon"></div>
				<div class="hover-desc">
					<h5>Retro Shoes</h5>
					<span>Clothing</span>
				</div>
			</div>
		</a>

		<div class="og-expander">
			<div class="flexslider gridslider">
				<ul class="slides">
					<li><img src="<?php echo base_url() ?>images/portfolio/portfolio-01a.jpg" alt=""></li>
				</ul>
			</div>

			<span class="og-close"></span>
			<div class="og-details">
				<h3>Retro Shoes</h3>
				<p>Nullam mollis sed purus id placerat. Aliquam non sem dui. Aenean ac consequat sem. Duis in interdum massa. Etiam sodales, est non luctus mollis, nisl nisi aliquam.</p>
				<p>Donec sit amet luctus nulla. Etiam bibendum fringilla convallis. Integer porta mi lorem, ut malesuada lorem vehicula lorem dolor. Morbi vitae egestas dolor.</p>

				<!-- Share Buttons -->	
				<div class="share-buttons">
					<ul>
						<li><a href="#">Share</a></li>
						<li class="share-facebook"><a href="#">Facebook</a></li>
						<li class="share-twitter"><a href="#">Twitter</a></li>
						<li class="share-gplus"><a href="#">Google Plus</a></li>
						<li class="share-pinit"><a href="#">Pin It</a></li>
					</ul>
				</div>

			</div>
		</div>
	</li>

	<!-- Project -->
	<li class="ccessories clothing technology clickable">


		<a href="#" class="grid-item-image">
			<div class="dynamic-portfolio-holder">
				<img src="<?php echo base_url() ?>images/portfolio/portfolio-02.jpg" alt=""/>
				<div class="hover-cover"></div>
				<div class="hover-icon"></div>
				<div class="hover-desc">
					<h5>Luxury Watch</h5>
					<span>Accessories</span>
				</div>
			</div>
		</a>

		<div class="og-expander">
			<div class="flexslider gridslider">
				<ul class="slides">
					<li><img src="<?php echo base_url() ?>images/portfolio/portfolio-02a.jpg" alt=""></li>
				</ul>
			</div>

			<span class="og-close"></span>
			<div class="og-details">
				<h3>Luxury Watch</h3>
				<p>Nullam mollis sed purus id placerat. Aliquam non sem dui. Aenean ac consequat sem. Duis in interdum massa. Etiam sodales, est non luctus mollis, nisl nisi aliquam.</p>
				<p>Donec sit amet luctus nulla. Etiam bibendum fringilla convallis. Integer porta mi lorem, ut malesuada lorem vehicula lorem dolor. Morbi vitae egestas dolor.</p>

				<!-- Share Buttons -->	
				<div class="share-buttons">
					<ul>
						<li><a href="#">Share</a></li>
						<li class="share-facebook"><a href="#">Facebook</a></li>
						<li class="share-twitter"><a href="#">Twitter</a></li>
						<li class="share-gplus"><a href="#">Google Plus</a></li>
						<li class="share-pinit"><a href="#">Pin It</a></li>
					</ul>
				</div>

			</div>
		</div>
	</li>

	<!-- Project -->
	<li class="clothing accessories clickable">

		<a href="#" class="grid-item-image">
			<div class="dynamic-portfolio-holder">
				<img src="<?php echo base_url() ?>images/portfolio/portfolio-03.jpg" alt=""/>
				<div class="hover-cover"></div>
				<div class="hover-icon"></div>
				<div class="hover-desc">
					<h5>Coat Button</h5>
					<span>Clothing</span>
				</div>
			</div>
		</a>

		<div class="og-expander">
			<div class="flexslider gridslider">
				<ul class="slides">
					<li><img src="<?php echo base_url() ?>images/portfolio/portfolio-03a.jpg" alt=""></li>
					<li><img src="<?php echo base_url() ?>images/portfolio/portfolio-03b.jpg" alt=""></li>
				</ul>
			</div>

			<span class="og-close"></span>
			<div class="og-details">
				<h3>Coat Button</h3>
				<p>Nullam mollis sed purus id placerat. Aliquam non sem dui. Aenean ac consequat sem. Duis in interdum massa. Etiam sodales, est non luctus mollis, nisl nisi aliquam.</p>
				<p>Donec sit amet luctus nulla. Etiam bibendum fringilla convallis. Integer porta mi lorem, ut malesuada lorem vehicula lorem dolor. Morbi vitae egestas dolor.</p>

				<!-- Share Buttons -->	
				<div class="share-buttons">
					<ul>
						<li><a href="#">Share</a></li>
						<li class="share-facebook"><a href="#">Facebook</a></li>
						<li class="share-twitter"><a href="#">Twitter</a></li>
						<li class="share-gplus"><a href="#">Google Plus</a></li>
						<li class="share-pinit"><a href="#">Pin It</a></li>
					</ul>
				</div>
				
			</div>
		</div>
	</li>

	<!-- Project -->
	<li class="other food clickable">

		<a href="#" class="grid-item-image">
			<div class="dynamic-portfolio-holder">
				<img src="<?php echo base_url() ?>images/portfolio/portfolio-04.jpg" alt=""/>
				<div class="hover-cover"></div>
				<div class="hover-icon"></div>
				<div class="hover-desc">
					<h5>Green Leaf</h5>
					<span>Other</span>
				</div>
			</div>
		</a>

		<div class="og-expander">
			<div class="flexslider gridslider">
				<ul class="slides">
					<li><img src="<?php echo base_url() ?>images/portfolio/portfolio-04a.jpg" alt=""></li>
				</ul>
			</div>

			<span class="og-close"></span>
			<div class="og-details">
				<h3>Green Leaf</h3>
				<p>Nullam mollis sed purus id placerat. Aliquam non sem dui. Aenean ac consequat sem. Duis in interdum massa. Etiam sodales, est non luctus mollis, nisl nisi aliquam.</p>
				<p>Donec sit amet luctus nulla. Etiam bibendum fringilla convallis. Integer porta mi lorem, ut malesuada lorem vehicula lorem dolor. Morbi vitae egestas dolor.</p>

				<!-- Share Buttons -->	
				<div class="share-buttons">
					<ul>
						<li><a href="#">Share</a></li>
						<li class="share-facebook"><a href="#">Facebook</a></li>
						<li class="share-twitter"><a href="#">Twitter</a></li>
						<li class="share-gplus"><a href="#">Google Plus</a></li>
						<li class="share-pinit"><a href="#">Pin It</a></li>
					</ul>
				</div>
				
			</div>
		</div>
	</li>

	<!-- Project -->
	<li class="food other accessories clickable">

		<a href="#" class="grid-item-image">
			<div class="dynamic-portfolio-holder">
				<img src="<?php echo base_url() ?>images/portfolio/portfolio-05.jpg" alt=""/>
				<div class="hover-cover"></div>
				<div class="hover-icon"></div>
				<div class="hover-desc">
					<h5>Raspberries</h5>
					<span>Food</span>
				</div>
			</div>
		</a>

		<div class="og-expander">
			<div class="flexslider gridslider">
				<ul class="slides">
					<li><img src="<?php echo base_url() ?>images/portfolio/portfolio-05a.jpg" alt=""></li>
				</ul>
			</div>

			<span class="og-close"></span>
			<div class="og-details">
				<h3>Raspberries</h3>
				<p>Nullam mollis sed purus id placerat. Aliquam non sem dui. Aenean ac consequat sem. Duis in interdum massa. Etiam sodales, est non luctus mollis, nisl nisi aliquam.</p>
				<p>Donec sit amet luctus nulla. Etiam bibendum fringilla convallis. Integer porta mi lorem, ut malesuada lorem vehicula lorem dolor. Morbi vitae egestas dolor.</p>

				<!-- Share Buttons -->	
				<div class="share-buttons">
					<ul>
						<li><a href="#">Share</a></li>
						<li class="share-facebook"><a href="#">Facebook</a></li>
						<li class="share-twitter"><a href="#">Twitter</a></li>
						<li class="share-gplus"><a href="#">Google Plus</a></li>
						<li class="share-pinit"><a href="#">Pin It</a></li>
					</ul>
				</div>
				
			</div>
		</div>
	</li>

	<!-- Project -->
	<li class="accessories clothing technology clickable">

		<a href="#" class="grid-item-image">
			<div class="dynamic-portfolio-holder">
				<img src="<?php echo base_url() ?>images/portfolio/portfolio-06.jpg" alt=""/>
				<div class="hover-cover"></div>
				<div class="hover-icon"></div>
				<div class="hover-desc">
					<h5>Fashion Glasses</h5>
					<span>Accessories</span>
				</div>
			</div>
		</a>

		<div class="og-expander">
			<div class="flexslider gridslider">
				<ul class="slides">
					<li><img src="<?php echo base_url() ?>images/portfolio/portfolio-06a.jpg" alt=""></li>
					<li><img src="<?php echo base_url() ?>images/portfolio/portfolio-06b.jpg" alt=""></li>
				</ul>
			</div>

			<span class="og-close"></span>
			<div class="og-details">
				<h3>Fashion Glasses</h3>
				<p>Nullam mollis sed purus id placerat. Aliquam non sem dui. Aenean ac consequat sem. Duis in interdum massa. Etiam sodales, est non luctus mollis, nisl nisi aliquam.</p>
				<p>Donec sit amet luctus nulla. Etiam bibendum fringilla convallis. Integer porta mi lorem, ut malesuada lorem vehicula lorem dolor. Morbi vitae egestas dolor.</p>

				<!-- Share Buttons -->	
				<div class="share-buttons">
					<ul>
						<li><a href="#">Share</a></li>
						<li class="share-facebook"><a href="#">Facebook</a></li>
						<li class="share-twitter"><a href="#">Twitter</a></li>
						<li class="share-gplus"><a href="#">Google Plus</a></li>
						<li class="share-pinit"><a href="#">Pin It</a></li>
					</ul>
				</div>
				
			</div>
		</div>
	</li>

	<!-- Project -->
	<li class="technology other clickable">

		<a href="#" class="grid-item-image">
			<div class="dynamic-portfolio-holder">
				<img src="<?php echo base_url() ?>images/portfolio/portfolio-07.jpg" alt=""/>
				<div class="hover-cover"></div>
				<div class="hover-icon"></div>
				<div class="hover-desc">
					<h5>RPM Tachometer</h5>
					<span>Technology</span>
				</div>
			</div>
		</a>

		<div class="og-expander">
			<div class="flexslider gridslider">
				<ul class="slides">
					<li><img src="<?php echo base_url() ?>images/portfolio/portfolio-07a.jpg" alt=""></li>
				</ul>
			</div>

			<span class="og-close"></span>
			<div class="og-details">
				<h3>RPM Tachometer</h3>
				<p>Nullam mollis sed purus id placerat. Aliquam non sem dui. Aenean ac consequat sem. Duis in interdum massa. Etiam sodales, est non luctus mollis, nisl nisi aliquam.</p>
				<p>Donec sit amet luctus nulla. Etiam bibendum fringilla convallis. Integer porta mi lorem, ut malesuada lorem vehicula lorem dolor. Morbi vitae egestas dolor.</p>

				<!-- Share Buttons -->	
				<div class="share-buttons">
					<ul>
						<li><a href="#">Share</a></li>
						<li class="share-facebook"><a href="#">Facebook</a></li>
						<li class="share-twitter"><a href="#">Twitter</a></li>
						<li class="share-gplus"><a href="#">Google Plus</a></li>
						<li class="share-pinit"><a href="#">Pin It</a></li>
					</ul>
				</div>
				
			</div>
		</div>
	</li>

	<!-- Project -->
	<li class="technology other clickable">

		<a href="#" class="grid-item-image">
			<div class="dynamic-portfolio-holder">
				<img src="<?php echo base_url() ?>images/portfolio/portfolio-08.jpg" alt=""/>
				<div class="hover-cover"></div>
				<div class="hover-icon"></div>
				<div class="hover-desc">
					<h5>Old Plane Propeller</h5>
					<span>Technology</span>
				</div>
			</div>
		</a>

		<div class="og-expander">
			<div class="flexslider gridslider">
				<ul class="slides">
						<li><img src="<?php echo base_url() ?>images/portfolio/portfolio-08a.jpg" alt=""></li>
						<li><img src="<?php echo base_url() ?>images/portfolio/portfolio-08b.jpg" alt=""></li>
				</ul>
			</div>

			<span class="og-close"></span>
			<div class="og-details">
				<h3>Old Plane Propeller</h3>
				<p>Nullam mollis sed purus id placerat. Aliquam non sem dui. Aenean ac consequat sem. Duis in interdum massa. Etiam sodales, est non luctus mollis, nisl nisi aliquam.</p>
				<p>Donec sit amet luctus nulla. Etiam bibendum fringilla convallis. Integer porta mi lorem, ut malesuada lorem vehicula lorem dolor. Morbi vitae egestas dolor.</p>

				<!-- Share Buttons -->	
				<div class="share-buttons">
					<ul>
						<li><a href="#">Share</a></li>
						<li class="share-facebook"><a href="#">Facebook</a></li>
						<li class="share-twitter"><a href="#">Twitter</a></li>
						<li class="share-gplus"><a href="#">Google Plus</a></li>
						<li class="share-pinit"><a href="#">Pin It</a></li>
					</ul>
				</div>
				
			</div>
		</div>
	</li>

	<!-- Project -->
	<li class="other clickable">

		<a href="#" class="grid-item-image">
			<div class="dynamic-portfolio-holder">
				<img src="<?php echo base_url() ?>images/portfolio/portfolio-09.jpg" alt=""/>
				<div class="hover-cover"></div>
				<div class="hover-icon"></div>
				<div class="hover-desc">
					<h5>Little Pitbull’s Look</h5>
					<span>Other</span>
				</div>
			</div>
		</a>

		<div class="og-expander">
			<div class="flexslider gridslider">
				<ul class="slides">
					<li><img src="<?php echo base_url() ?>images/portfolio/portfolio-09a.jpg" alt=""></li>
					<li><img src="<?php echo base_url() ?>images/portfolio/portfolio-09b.jpg" alt=""></li>
				</ul>
			</div>

			<span class="og-close"></span>
			<div class="og-details">
				<h3>Little Pitbull’s Look</h3>
				<p>Nullam mollis sed purus id placerat. Aliquam non sem dui. Aenean ac consequat sem. Duis in interdum massa. Etiam sodales, est non luctus mollis, nisl nisi aliquam.</p>
				<p>Donec sit amet luctus nulla. Etiam bibendum fringilla convallis. Integer porta mi lorem, ut malesuada lorem vehicula lorem dolor. Morbi vitae egestas dolor.</p>

				<!-- Share Buttons -->	
				<div class="share-buttons">
					<ul>
						<li><a href="#">Share</a></li>
						<li class="share-facebook"><a href="#">Facebook</a></li>
						<li class="share-twitter"><a href="#">Twitter</a></li>
						<li class="share-gplus"><a href="#">Google Plus</a></li>
						<li class="share-pinit"><a href="#">Pin It</a></li>
					</ul>
				</div>
				
			</div>
		</div>
	</li>

	<!-- Project -->
	<li class="other technology clickable">


		<a href="#" class="grid-item-image">
			<div class="dynamic-portfolio-holder">
				<img src="<?php echo base_url() ?>images/portfolio/portfolio-10.jpg" alt=""/>
				<div class="hover-cover"></div>
				<div class="hover-icon"></div>
				<div class="hover-desc">
					<h5>Modern Building</h5>
					<span>Other</span>
				</div>
			</div>
		</a>

		<div class="og-expander">
			<div class="flexslider gridslider">
				<ul class="slides">
					<li><img src="<?php echo base_url() ?>images/portfolio/portfolio-10a.jpg" alt=""></li>
				</ul>
			</div>

			<span class="og-close"></span>
			<div class="og-details">
				<h3>Modern Building</h3>
				<p>Nullam mollis sed purus id placerat. Aliquam non sem dui. Aenean ac consequat sem. Duis in interdum massa. Etiam sodales, est non luctus mollis, nisl nisi aliquam.</p>
				<p>Donec sit amet luctus nulla. Etiam bibendum fringilla convallis. Integer porta mi lorem, ut malesuada lorem vehicula lorem dolor. Morbi vitae egestas dolor.</p>

				<!-- Share Buttons -->	
				<div class="share-buttons">
					<ul>
						<li><a href="#">Share</a></li>
						<li class="share-facebook"><a href="#">Facebook</a></li>
						<li class="share-twitter"><a href="#">Twitter</a></li>
						<li class="share-gplus"><a href="#">Google Plus</a></li>
						<li class="share-pinit"><a href="#">Pin It</a></li>
					</ul>
				</div>
				
			</div>
		</div>
	</li>

	<!-- Project -->
	<li class="other accessories clickable">

		<a href="#" class="grid-item-image">
			<div class="dynamic-portfolio-holder">
				<img src="<?php echo base_url() ?>images/portfolio/portfolio-11.jpg" alt=""/>
				<div class="hover-cover"></div>
				<div class="hover-icon"></div>
				<div class="hover-desc">
					<h5>Pine Cone in Forest</h5>
					<span>Other</span>
				</div>
			</div>
		</a>

		<div class="og-expander">
			<div class="flexslider gridslider">
				<ul class="slides">
					<li><img src="<?php echo base_url() ?>images/portfolio/portfolio-11a.jpg" alt=""></li>
				</ul>
			</div>

			<span class="og-close"></span>
			<div class="og-details">
				<h3>Pine Cone in Forest</h3>
				<p>Nullam mollis sed purus id placerat. Aliquam non sem dui. Aenean ac consequat sem. Duis in interdum massa. Etiam sodales, est non luctus mollis, nisl nisi aliquam.</p>
				<p>Donec sit amet luctus nulla. Etiam bibendum fringilla convallis. Integer porta mi lorem, ut malesuada lorem vehicula lorem dolor. Morbi vitae egestas dolor.</p>

				<!-- Share Buttons -->	
				<div class="share-buttons">
					<ul>
						<li><a href="#">Share</a></li>
						<li class="share-facebook"><a href="#">Facebook</a></li>
						<li class="share-twitter"><a href="#">Twitter</a></li>
						<li class="share-gplus"><a href="#">Google Plus</a></li>
						<li class="share-pinit"><a href="#">Pin It</a></li>
					</ul>
				</div>
				
			</div>
		</div>
	</li>

	<!-- Project -->
	<li class="accessories clothing clickable">

		<a href="#" class="grid-item-image">
			<div class="dynamic-portfolio-holder">
				<img src="<?php echo base_url() ?>images/portfolio/portfolio-12.jpg" alt=""/>
				<div class="hover-cover"></div>
				<div class="hover-icon"></div>
				<div class="hover-desc">
					<h5>Headphone Jack</h5>
					<span>Technology</span>
				</div>
			</div>
		</a>

		<div class="og-expander">
			<div class="flexslider gridslider">
				<ul class="slides">
					<li><img src="<?php echo base_url() ?>images/portfolio/portfolio-12a.jpg" alt=""></li>
				</ul>
			</div>

			<span class="og-close"></span>
			<div class="og-details">
				<h3>Headphone Jack</h3>
				<p>Nullam mollis sed purus id placerat. Aliquam non sem dui. Aenean ac consequat sem. Duis in interdum massa. Etiam sodales, est non luctus mollis, nisl nisi aliquam.</p>
				<p>Donec sit amet luctus nulla. Etiam bibendum fringilla convallis. Integer porta mi lorem, ut malesuada lorem vehicula lorem dolor. Morbi vitae egestas dolor.</p>
				
				<!-- Share Buttons -->	
				<div class="share-buttons">
					<ul>
						<li><a href="#">Share</a></li>
						<li class="share-facebook"><a href="#">Facebook</a></li>
						<li class="share-twitter"><a href="#">Twitter</a></li>
						<li class="share-gplus"><a href="#">Google Plus</a></li>
						<li class="share-pinit"><a href="#">Pin It</a></li>
					</ul>
				</div>
				
			</div>
		</div>
	</li>

</ul>

</div>
<!-- Container / End -->
