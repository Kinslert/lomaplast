<div class="clearfix"></div>


<div class="content">
<div id="canvas">

<div class="zoom-icon zoom-icon-in"></div>

<div class="magazine-viewport">
  <div class="container">
    <div class="magazine">
      <!-- Next button -->
      <div ignore="1" class="next-button"></div>
      <!-- Previous button -->
      <div ignore="1" class="previous-button"></div>
    </div>
  </div>
<!-- <div class="bottom">
  <div id="slider-bar" class="turnjs-slider">
    <div id="slider"></div>
  </div>
</div>  -->
</div>
</div>
</div>

<script type="text/javascript">

function loadApp() {

  $('#canvas').fadeIn(1000);

  var flipbook = $('.magazine');

  // Check if the CSS was already loaded
  
  if (flipbook.width()==0 || flipbook.height()==0) {
    setTimeout(loadApp, 10);
    return;
  }
  
  // Create the flipbook

  flipbook.turn({
      
      // Magazine width

      width: 922,

      // Magazine height

      height: 600,

      // Duration in millisecond

      duration: 1000,

      // Enables gradients

      gradients: true,
      
      // Auto center this flipbook

      autoCenter: true,

      // Elevation from the edge of the flipbook when turning a page

      elevation: 50,

      // The number of pages

      pages: 53,

      // Events

      when: {
        turning: function(event, page, view) {
          
          var book = $(this),
          currentPage = book.turn('page'),
          pages = book.turn('pages');
      
          // Update the current URI

          Hash.go('page/' + page).update();

          // Show and hide navigation buttons

          disableControls(page);

        },

        turned: function(event, page, view) {

          disableControls(page);

          $(this).turn('center');

          $('#slider').slider('value', getViewNumber($(this), page));

          if (page==1) { 
            $(this).turn('peel', 'br');
          }

        },

        missing: function (event, pages) {

          // Add pages that aren't in the magazine

          for (var i = 0; i < pages.length; i++)
            addPage(pages[i], $(this));

        }
      }

  });

  // Zoom.js

  $('.magazine-viewport').zoom({
    flipbook: $('.magazine'),

    max: function() { 
      
      return largeMagazineWidth()/$('.magazine').width();

    }, 

    when: {
      swipeLeft: function() {

        $(this).zoom('flipbook').turn('next');

      },

      swipeRight: function() {
        
        $(this).zoom('flipbook').turn('previous');

      },

      resize: function(event, scale, page, pageElement) {

        if (scale==1)
          loadSmallPage(page, pageElement);
        else
          loadLargePage(page, pageElement);

      },

      zoomIn: function () {

        $('#slider-bar').hide();
        $('.made').hide();
        $('.magazine').removeClass('animated').addClass('zoom-in');
        $('.zoom-icon').removeClass('zoom-icon-in').addClass('zoom-icon-out');
        
        if (!window.escTip && !$.isTouch) {
          escTip = true;

          $('<div />', {'class': 'exit-message'}).
            html('<div>Press ESC to exit</div>').
              appendTo($('body')).
              delay(2000).
              animate({opacity:0}, 500, function() {
                $(this).remove();
              });
        }
      },

      zoomOut: function () {

        $('#slider-bar').fadeIn();
        $('.exit-message').hide();
        $('.made').fadeIn();
        $('.zoom-icon').removeClass('zoom-icon-out').addClass('zoom-icon-in');

        setTimeout(function(){
          $('.magazine').addClass('animated').removeClass('zoom-in');
          resizeViewport();
        }, 0);

      }
    }
  });

  // Zoom event

  if ($.isTouch)
    $('.magazine-viewport').bind('zoom.doubleTap', zoomTo);
  else
    $('.magazine-viewport').bind('zoom.tap', zoomTo);


  // Using arrow keys to turn the page

  $(document).keydown(function(e){

    var previous = 37, next = 39, esc = 27;

    switch (e.keyCode) {
      case previous:

        // left arrow
        $('.magazine').turn('previous');
        e.preventDefault();

      break;
      case next:

        //right arrow
        $('.magazine').turn('next');
        e.preventDefault();

      break;
      case esc:
        
        $('.magazine-viewport').zoom('zoomOut');  
        e.preventDefault();

      break;
    }
  });

  // URIs - Format #/page/1 

  Hash.on('^page\/([0-9]*)$', {
    yep: function(path, parts) {
      var page = parts[1];

      if (page!==undefined) {
        if ($('.magazine').turn('is'))
          $('.magazine').turn('page', page);
      }

    },
    nop: function(path) {

      if ($('.magazine').turn('is'))
        $('.magazine').turn('page', 1);
    }
  });


  $(window).resize(function() {
    resizeViewport();
  }).bind('orientationchange', function() {
    resizeViewport();
  });

  // Regions

  if ($.isTouch) {
    $('.magazine').bind('touchstart', regionClick);
  } else {
    $('.magazine').click(regionClick);
  }

  // Events for the next button

  $('.next-button').bind($.mouseEvents.over, function() {
    
    $(this).addClass('next-button-hover');

  }).bind($.mouseEvents.out, function() {
    
    $(this).removeClass('next-button-hover');

  }).bind($.mouseEvents.down, function() {
    
    $(this).addClass('next-button-down');

  }).bind($.mouseEvents.up, function() {
    
    $(this).removeClass('next-button-down');

  }).click(function() {
    
    $('.magazine').turn('next');

  });

  // Events for the next button
  
  $('.previous-button').bind($.mouseEvents.over, function() {
    
    $(this).addClass('previous-button-hover');

  }).bind($.mouseEvents.out, function() {
    
    $(this).removeClass('previous-button-hover');

  }).bind($.mouseEvents.down, function() {
    
    $(this).addClass('previous-button-down');

  }).bind($.mouseEvents.up, function() {
    
    $(this).removeClass('previous-button-down');

  }).click(function() {
    
    $('.magazine').turn('previous');

  });


  // Slider

  $( "#slider" ).slider({
    min: 1,
    max: numberOfViews(flipbook),

    start: function(event, ui) {

      if (!window._thumbPreview) {
        _thumbPreview = $('<div />', {'class': 'thumbnail'}).html('<div></div>');
        setPreview(ui.value);
        _thumbPreview.appendTo($(ui.handle));
      } else
        setPreview(ui.value);

      moveBar(false);

    },

    slide: function(event, ui) {

      setPreview(ui.value);

    },

    stop: function() {

      if (window._thumbPreview)
        _thumbPreview.removeClass('show');
      
      $('.magazine').turn('page', Math.max(1, $(this).slider('value')*2 - 2));

    }
  });

  resizeViewport();

  $('.magazine').addClass('animated');

}

// Zoom icon

 $('.zoom-icon').bind('mouseover', function() { 
  
  if ($(this).hasClass('zoom-icon-in'))
    $(this).addClass('zoom-icon-in-hover');

  if ($(this).hasClass('zoom-icon-out'))
    $(this).addClass('zoom-icon-out-hover');
 
 }).bind('mouseout', function() { 
  
   if ($(this).hasClass('zoom-icon-in'))
    $(this).removeClass('zoom-icon-in-hover');
  
  if ($(this).hasClass('zoom-icon-out'))
    $(this).removeClass('zoom-icon-out-hover');

 }).bind('click', function() {

  if ($(this).hasClass('zoom-icon-in'))
    $('.magazine-viewport').zoom('zoomIn');
  else if ($(this).hasClass('zoom-icon-out')) 
    $('.magazine-viewport').zoom('zoomOut');

 });

 $('#canvas').hide();


// Load the HTML4 version if there's not CSS transform

yepnope({
  test : Modernizr.csstransforms,
  yep: ['lib/turn.min.js'],
  nope: ['lib/turn.html4.min.js', 'css/jquery.ui.html4.css'],
  both: ['lib/zoom.min.js', 'css/jquery.ui.css', 'js/magazine.js', 'css/magazine.css'],
  complete: loadApp
});

</script>



<!-- 
<div class="container">
<div class="six-columns">
  <div style="width:960px;height:700px;" >
  <div id="arrow_previus" style="float:left;width:36px;height:36px;background-image:url('images/catalogo/arrrow_left_blue.png');margin-top:260px;cursor:pointer;display:none;" ></div>
  <div id="arrow_next"  style="float:right;width:36px;height:36px;background-image:url('images/catalogo/arrrow_right_blue.png');margin-top:260px;cursor:pointer;margin-right:220px;display:block;" ></div>
  <div  id="magazine_frame" align="left" >
    <div align="left" id="magazine" style="" >
              <div style="background-image:url(images/catalogo/00010.jpg);"></div>
              <div style="background-image:url(images/catalogo/00020.jpg);"></div>
              <div style="background-image:url(images/catalogo/00030.jpg);"></div>
              <div style="background-image:url(images/catalogo/00040.jpg);"></div>
              <div style="background-image:url(images/catalogo/00050.jpg);"></div>
              <div style="background-image:url(images/catalogo/00060.jpg);"></div>
              <div style="background-image:url(images/catalogo/00070.jpg);"></div>
              <div style="background-image:url(images/catalogo/00080.jpg);"></div>
              <div style="background-image:url(images/catalogo/00090.jpg);"></div>
              <div style="background-image:url(images/catalogo/00100.jpg);"></div>
              <div style="background-image:url(images/catalogo/00110.jpg);"></div>
              <div style="background-image:url(images/catalogo/00120.jpg);"></div>
              <div style="background-image:url(images/catalogo/00130.jpg);"></div>
              <div style="background-image:url(images/catalogo/00140.jpg);"></div>
              <div style="background-image:url(images/catalogo/00150.jpg);"></div>
              <div style="background-image:url(images/catalogo/00160.jpg);"></div>
              <div style="background-image:url(images/catalogo/00170.jpg);"></div>
              <div style="background-image:url(images/catalogo/00180.jpg);"></div>
              <div style="background-image:url(images/catalogo/00190.jpg);"></div>
              <div style="background-image:url(images/catalogo/00200.jpg);"></div>
              <div style="background-image:url(images/catalogo/00210.jpg);"></div>
              <div style="background-image:url(images/catalogo/00211.jpg);"></div>
              <div style="background-image:url(images/catalogo/00220.jpg);"></div>
              <div style="background-image:url(images/catalogo/00230.jpg);"></div>
              <div style="background-image:url(images/catalogo/00240.jpg);"></div>
              <div style="background-image:url(images/catalogo/00250.jpg);"></div>
              <div style="background-image:url(images/catalogo/00260.jpg);"></div>
              <div style="background-image:url(images/catalogo/00270.jpg);"></div>
              <div style="background-image:url(images/catalogo/00280.jpg);"></div>
              <div style="background-image:url(images/catalogo/00290.jpg);"></div>
              <div style="background-image:url(images/catalogo/00300.jpg);"></div>
              <div style="background-image:url(images/catalogo/00310.jpg);"></div>
              <div style="background-image:url(images/catalogo/00320.jpg);"></div>
              <div style="background-image:url(images/catalogo/00330.jpg);"></div>
              <div style="background-image:url(images/catalogo/00340.jpg);"></div>
              <div style="background-image:url(images/catalogo/00350.jpg);"></div>
              <div style="background-image:url(images/catalogo/00360.jpg);"></div>
              <div style="background-image:url(images/catalogo/00370.jpg);"></div>
              <div style="background-image:url(images/catalogo/00380.jpg);"></div>
              <div style="background-image:url(images/catalogo/00390.jpg);"></div>
              <div style="background-image:url(images/catalogo/00400.jpg);"></div>
              <div style="background-image:url(images/catalogo/00410.jpg);"></div>
              <div style="background-image:url(images/catalogo/00420.jpg);"></div>
              <div style="background-image:url(images/catalogo/00430.jpg);"></div>
              <div style="background-image:url(images/catalogo/00440.jpg);"></div>
              <div style="background-image:url(images/catalogo/00450.jpg);"></div>
              <div style="background-image:url(images/catalogo/00460.jpg);"></div>
              <div style="background-image:url(images/catalogo/00470.jpg);"></div>
              <div style="background-image:url(images/catalogo/00480.jpg);"></div>
              <div style="background-image:url(images/catalogo/00490.jpg);"></div>
              <div style="background-image:url(images/catalogo/00500.jpg);"></div>
              <div style="background-image:url(images/catalogo/00510.jpg);"></div>
              <div style="background-image:url(images/catalogo/00520.jpg);"></div>
              <div style="background-image:url(images/catalogo/00530.jpg);"></div>
</div>
</div>
  </div>
   </div>
            </div> -->




     

              



