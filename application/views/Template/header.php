<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
$this->lang->load('french');
$this->lang->load('english');
$this->lang->load('portuguese');
?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="<?php if($this->uri->segment(1) == 'es'){ echo 'es';}else if($this->uri->segment(1) == 'en' ){ echo 'en';}elseif ($this->uri->segment(1) == 'fr') { echo 'fr';}else if($this->uri->segment(1) == 'pt'){echo 'pt';}else{echo 'es';}?>"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="<?php if($this->uri->segment(1) == 'es'){ echo 'es';}else if($this->uri->segment(1) == 'en' ){ echo 'en';}elseif ($this->uri->segment(1) == 'fr') { echo 'fr';}else if($this->uri->segment(1) == 'pt'){echo 'pt';}else{echo 'es';}?>"> <!--<![endif]-->
<head>

<!-- Basic Page Needs
================================================== -->
<meta charset="utf-8">
<title><?php echo $title; ?></title>

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


<!-- CSS
================================================== -->
<link rel="stylesheet" href="<?php echo base_url();?>css/style.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/jquery.ui.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/jquery.ui.html4.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/magazine.css">

<link rel="stylesheet" href="<?php echo base_url();?>css/colors/blue.css" id="colors">
<script type="text/javascript" src=" <?php echo base_url() ?>extras/jquery.min.1.7.js"></script>
<script type="text/javascript" src=" <?php echo base_url() ?>extras/jquery-ui-1.8.20.custom.min.js"></script>
<script type="text/javascript" src=" <?php echo base_url() ?>extras/modernizr.2.5.3.min.js"></script>
<script type="text/javascript" src=" <?php echo base_url() ?>lib/hash.js"></script>
 
<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<link href="<?php echo base_url() ?>css/jquery.bxslider.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url() ?>css/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
</head>

<body class="boxed">
<div id="wrapper">


<!-- Top Bar
================================================== -->
<div id="top-bar">
	<div class="container">

		<!-- Top Bar Menu -->
		<div class="ten columns">
			<ul class="top-bar-menu">
				<li><i class="fa fa-phone"></i> 00 54 11 4767 5515</li>
				<li><i class="fa fa-envelope"></i> <a href="#">info@lomaplast.com</a></li>
				<li>
					<div class="top-bar-dropdown">
						<?php 
							switch ($this->uri->segment(1)) {
								case '':
									echo '<span>'.'Spanish'.'</span>';
									break;
									case 'en':
									echo '<span>'.'English'.'</span>';
									break;
									case 'fr':
									echo '<span>'.'Français'.'</span>';
									break;
									case 'pt':
									echo '<span>'.'Portuguese'.'</span>';
									break;
								default:
									echo '<span>'.'Español'.'</span>';
									break;
							}
						?>
						<ul class="options">
						<?php /* variables Menu */ 
						$contact = 'contact';
						$noticias = 'noticias';
						$empresa = 'about';
						$catalogo = 'catalogo';
						$productos = 'categorias';
						$ford = 'ford';
						$fiat = 'fiat';
						$chevrolet = 'chevrolet';
						$toyota = 'toyota';
						$renault = 'renault';
						$volkswagen = 'volkswagen';
						?>
							<li><div class="arrow"></div></li>
							<li>
							<?php if( $this->uri->segment(1) == '' || $this->uri->segment(1) == $contact|| $this->uri->segment(1) == $contact|| $this->uri->segment(1) == $contact|| $this->uri->segment(1) == $noticias|| $this->uri->segment(1) == $empresa|| $this->uri->segment(1) == $catalogo|| $this->uri->segment(1) == $productos|| $this->uri->segment(2) == $ford || $this->uri->segment(2) == $fiat|| $this->uri->segment(2) == $chevrolet|| $this->uri->segment(2) == $toyota|| $this->uri->segment(2) == $renault || $this->uri->segment(2) == $volkswagen){ ?></li>
						 <li><?php echo anchor('en', 'English'); ?></li>
						 <li><?php echo anchor('pt', 'Portuguese');?></li>
						 <li><?php echo anchor('fr', 'Français');?></li>
						<?php
							}else if($this->uri->segment(1) == 'en'){
						?>
						<li><?php echo anchor('', 'Spanish'); ?></li>
						 <li><?php echo anchor('fr', 'Français'); ?></li>
						 <li><?php echo anchor('pt', 'Portuguese'); ?></li>
						<?php  }else if($this->uri->segment(1) == 'fr'){ ?>
						<li><?php echo anchor('', 'Spanish'); ?></li>
						 <li><?php echo anchor('en', 'English'); ?></li>
						 <li><?php echo anchor('pt', 'Portuguese'); ?></li>
						<?php  }else if($this->uri->segment(1) == 'pt'){ ?>
						<li><?php echo anchor('', 'Spanish'); ?></li>
						 <li><?php echo anchor('en', 'English'); ?></li>
						 <li><?php echo anchor('fr', 'Français'); ?></li>
							<?php } ?>
						</ul>
					</div>
				</li>
				<li>

				</li>
			</ul>
		</div>
		<!-- Social Icons -->
		<div class="six columns">

		</div>

	</div>
</div>

<div class="clearfix"></div>
<!-- home
================================================== -->


<!-- Header
================================================== -->
<div class="container">


	<!-- Logo -->
	<div class="four columns">
		<div id="">
			<h1><a href="<?php echo base_url(); ?>"><img class="logo_loma" src="<?php echo base_url(); ?>images/logo.jpg" alt="Loma Hermosa Plast" /></a></h1>
		</div>
	</div>


	<!-- Additional Menu -->
	<div class="twelve columns">
		<div id="additional-menu">

		</div>
	</div>


	<!-- Shopping Cart -->
	<div class="twelve columns">

		<!-- Search -->
		<nav class="top-search">
			<form action="#" method="get">
				<button><i class="fa fa-search"></i></button>
				<input class="search-field" type="text" placeholder="Buscar" value=""/>
			</form>
		</nav>

	</div>

</div>


<!-- Navigation
================================================== -->
<div class="container">
	<div class="sixteen columns">
		
		<a href="#menu" class="menu-trigger"><i class="fa fa-bars"></i> Menu</a>

		<nav id="navigation">
			<ul class="menu" id="responsive">

				<li><a href="<?php echo base_url(); ?>" id="current">Home</a></li>
				<?php $cat = base_url().'marca/';	 ?>
				<li class="dropdown">
					<a href="#">PRODUCTOS</a>
					<ul>
						<li><a href="<?php echo base_url(); ?>categorias">Ver marcas</a></li>
						<li><a href="<?php echo $cat ?>ford">Ford</a></li>
						<li><a href="<?php echo $cat ?>fiat">Fiat</a></li>
						<li><a href="<?php echo $cat ?>chevrolet">Chevrolet</a></li>
						<li><a href="<?php echo $cat ?>toyota">Toyota</a></li>
						<li><a href="<?php echo $cat ?>renault">Renault</a></li>
						<li><a href="<?php echo $cat ?>volkswagen">Volkswagen</a></li>
					</ul>
				</li>

				<li>
					<a href="<?php echo base_url(); ?>catalogo">CATÁLOGO</a>

				</li>

				<li><a href="<?php echo base_url(); ?>noticias">Noticias</a></li>
				
				<li class="demo-button">
				  <a href="<?php echo base_url(); ?>about">Empresa</a>
				</li>
				
				<li class="demo-button">
					<a href="<?php echo base_url(); ?>contact">Contacto</a>
				</li>
			</ul>
		</nav>
	</div>
</div>


<!-- Titlebar
================================================== -->

<?php 
if($this->uri->segment(1) == 'en'){
	$url_actual = false;
	
}else{
$url_actual = $this->uri->segment(1);
$categoria_actual = $this->uri->segment(1).'/'.$this->uri->segment(2);
$marca = $this->uri->segment(2);
if($url_actual == 'Home'  || $url_actual == '' || $url_actual == 'categorias' || $categoria_actual == 'marca/'.$marca || $url_actual == 'categoria'  || $url_actual == 'articulos' ){
	if($url_actual == 'categorias' || $categoria_actual == 'marca/'.$marca || $url_actual == 'categoria' || $url_actual == 'articulos'){
	?>
<div class="clearfix"></div>
<!-- New Arrivals
================================================== -->
<section class="parallax-titlebar fullwidth-element"  data-background="#000" data-opacity="0.45" data-height="160">

    <img src="<?php echo base_url(); ?>images/fotoprincipal.jpg" alt="" />
    <div class="parallax-overlay"></div>


    <div class="parallax-content">
        <h2>Marcas <span>Todos los productos de este sitio son reemplazos adaptables a todas las marcas originales.</span></h2>

	<nav id="breadcrumbs">
	<?php
	switch ($categoria_actual) {
		case 'ford':
		?>
		<h2>Productos hacer la inclusion de las paginas</h2> 
		<?php 
		case 'catalogo':
		?>
		<h2>Productos</h2>
		<?php
		break;
		case 'noticias':
		?>
		<h2>Noticias</h2> 
		<?php	
		break;
		case 'contact':
		?>
		<h2>Contacto</h2>
		<?php
		break;
		?>
		<?php 
		case 'about':
		?>
		<h2>La Empresa</h2>
		<?php
		break;
		default:
			# code...
			break;
	}
	// add breadcrumbs
	$this->breadcrumbs->push($url_actual,'categorias');
	$this->breadcrumbs->push($marca,'CATEGORIA_ACTUAL');
	// unshift crumb
	$this->breadcrumbs->unshift(anchor('','Home'), 'Home');
	// output
	echo $this->breadcrumbs->show();
?>
 </nav>
    </div>

</section>
	<?php
	}
return false;

}else{
?>
<section class="titlebar">
<div class="container">
<div class="sixteen columns">
<nav id="breadcrumbs">
<?php
switch ($url_actual) {
	case 'productos':
	?>
	<h2>Productos hacer la inclusion de las paginas</h2> 
	<?php 
	case 'catalogo':
	?>
	<h2>Productos</h2>
	<?php
	break;
	case 'noticias':
	?>
	<h2>Noticias</h2> 
	<?php	
	break;
	case 'contact':
	?>
	<h2>Contacto</h2>
	<?php
	break;
	?>
	<?php 
	case 'about':
	?>
	<h2>La Empresa</h2>
	<?php
	break;
	default:
		# code...
		break;
}
// add breadcrumbs
$this->breadcrumbs->push($url_actual,'PAGINA');
// unshift crumb
$this->breadcrumbs->unshift(anchor('','Home'), 'Home');
// output
echo $this->breadcrumbs->show();
?>
 </nav>
</div>
</div>
</section>
<?php
}
}
?>