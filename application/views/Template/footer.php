<div class="margin-top-50"></div>

<!-- Footer
================================================== -->
<div id="footer">

	<!-- Container -->
	<div class="container">

		<div class="four columns">
			<img style="height: 75px" src="<?php echo base_url(); ?>images/logo-loma.png" alt="" class="margin-top-10"/>
			<p class="margin-top-15">Todos los productos de este sitio son reemplazos adaptables a todas las marcas originales.</p>
		</div>

		<div class="four columns">

			<!-- Headline -->
			<h3 class="headline footer">Secciones</h3>
			<span class="line"></span>
			<div class="clearfix"></div>

			<ul class="footer-links">
				<li><a href="<?php echo base_url(); ?>catalogo">Catálogo</a></li>
				<li><a href="<?php echo base_url(); ?>contact">Dónde estamos</a></li>
				<li><a href="<?php echo base_url(); ?>contact">Contacto</a></li>
				<li><a href="<?php echo base_url(); ?>about">Empresa</a></li>
			</ul>

		</div>

		<div class="four columns">


		</div>

		<div class="four columns">

		</div>

	</div>
	<!-- Container / End -->

</div>
<!-- Footer / End -->

<!-- Footer Bottom / Start -->

<!-- Footer Bottom / End -->

<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>

</div>


<!-- Java Script
================================================== -->
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url();?>scripts/jquery.jpanelmenu.js"></script>
<script src="<?php echo base_url();?>scripts/jquery.themepunch.plugins.min.js"></script>
<script src="<?php echo base_url();?>scripts/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo base_url();?>scripts/jquery.themepunch.showbizpro.min.js"></script>
<script src="<?php echo base_url();?>scripts/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url();?>scripts/hoverIntent.js"></script>
<script src="<?php echo base_url();?>scripts/superfish.js"></script>
<script src="<?php echo base_url();?>scripts/jquery.pureparallax.js"></script>
<script src="<?php echo base_url();?>scripts/jquery.pricefilter.js"></script>
<script src="<?php echo base_url();?>scripts/jquery.selectric.min.js"></script>
<script src="<?php echo base_url();?>scripts/jquery.royalslider.min.js"></script>
<script src="<?php echo base_url();?>scripts/SelectBox.js"></script>
<script src="<?php echo base_url();?>scripts/modernizr.custom.js"></script>
<script src="<?php echo base_url();?>scripts/waypoints.min.js"></script>
<script src="<?php echo base_url();?>scripts/jquery.flexslider-min.js"></script>
<script src="<?php echo base_url();?>scripts/jquery.counterup.min.js"></script>
<script src="<?php echo base_url();?>scripts/jquery.tooltips.min.js"></script>
<script src="<?php echo base_url();?>scripts/jquery.isotope.min.js"></script>
<script src="<?php echo base_url();?>scripts/puregrid.js"></script>
<script src="<?php echo base_url();?>scripts/stacktable.js"></script>
<script src="<?php echo base_url();?>scripts/custom.js"></script>
<script src="<?php echo base_url(); ?>scripts/jquery.mousewheel.min.js" charset="utf-8" ></script>
<script src="<?php echo base_url(); ?>scripts/raphael-min.js" charset="utf-8" ></script>
<script src="<?php echo base_url(); ?>scripts/jquery.mapael.js"></script>
<script src="<?php echo base_url(); ?>scripts/maps/france_departments.js" charset="utf-8" ></script>
<script src="<?php echo base_url(); ?>scripts/maps/world_countries.js"></script>
<script src="<?php echo base_url(); ?>scripts/plugin-map-index.js"></script>
<script src="<?php echo base_url();?>scripts/jquery.gmaps.min.js"></script>
<script src="<?php echo base_url(); ?>scripts/jquery.validate.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/additional-methods.js"></script>
<script src="<?php echo base_url(); ?>scripts/validation.form.contact.js"></script>
<script src="<?php echo base_url(); ?>scripts/jquery.slides.min.js"></script>
<script type="text/javascript">
	(function($){
		$(document).ready(function(){

			$('#googlemaps').gMap({
				maptype: 'ROADMAP',
				scrollwheel: false,
				zoom: 13,
				markers: [
					{
						address: 'Saavedra 6652, Loma Hermosa, Buenos Aires, Argentina', // Your Adress Here
						html: '<strong>LOMA PLAST</strong><br>Saavedra 6652, Loma Hermosa </br>Buenos Aires, Argentina',
						popup: true,
					}
				],
			});

		   });

	})(this.jQuery);
</script>
<script src="<?php echo base_url(); ?>scripts/turn.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>scripts/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>scripts/turn.activate.js"></script>
</body>
</html>