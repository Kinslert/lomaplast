<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <title>Loma Hermosa Plast</title>

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/colors/blue.css" id="colors">

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body class="boxed">
<div id="wrapper">


<!-- Top Bar
================================================== -->
<div id="top-bar">
    <div class="container">

        <!-- Top Bar Menu -->
        <div class="ten columns">
            <ul class="top-bar-menu">
                <li><i class="fa fa-phone"></i> 00 54 11 4767 5515</li>
                <li><i class="fa fa-envelope"></i> <a href="#">info@lomaplast.com</a></li>
                <li>
                </li>
                <li>

                </li>
            </ul>
        </div>

        <!-- Social Icons -->
        <div class="six columns">

        </div>

    </div>
</div>

<div class="clearfix"></div>


<!-- Header
================================================== -->
<div class="container">


    <!-- Logo -->
    <div class="four columns">
        <div id="">
            <h1><a href="index.html"><img class="logo_loma" src="<?php echo base_url(); ?>images/logo.jpg" alt="Loma Hermosa Plast" /></a></h1>
        </div>
    </div>


    <!-- Additional Menu -->
    <div class="twelve columns">
        <div id="additional-menu">

        </div>
    </div>


    <!-- Shopping Cart -->
    <div class="twelve columns">

        <!-- Search -->
        <nav class="top-search">
            <form action="#" method="get">
                <button><i class="fa fa-search"></i></button>
                <input class="search-field" type="text" placeholder="Search" value=""/>
            </form>
        </nav>

    </div>

</div>
    <div class="margin-top-50"></div>
<!-- Slider
================================================== -->
<div class="container fullwidth-element home-slider">

    <div class="tp-banner-container">
        <div class="tp-banner">
            <ul>
                <!-- Slide 1  -->
                <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                    <img src="<?php echo base_url(); ?>images/fotoprincipal.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                    <div class="caption sfb fadeout" data-x="145" data-y="170" data-speed="400" data-start="800"  data-easing="Power4.easeOut">
                        <h2>Confianza
                            y compromiso</h2>
                        <h3></h3>
                        <a href="<?php echo base_url() ?>pt" class="caption-btn">
                           PT
                        </a>
                        <a href="<?php echo base_url() ?>en" class="caption-btn">
                           EN
                        </a>
                        <a href="<?php echo base_url() ?>fr" class="caption-btn">
                            FR
                        </a>
                        <a href="<?php echo base_url() ?>" class="caption-btn">
                           ES
                        </a>
                    </div>
                </li>

            </ul>
        </div>
    </div>
</div>


<div class="clearfix"></div>


<!-- New Arrivals
================================================== -->
<div class="container">



    <!-- Carousel -->

</div>




<!-- Product Lists
================================================== -->

<div class="clearfix"></div>


<!-- Latest Articles
================================================== -->

<div class="margin-top-50"></div>

<!-- Footer Bottom / Start -->


</div>