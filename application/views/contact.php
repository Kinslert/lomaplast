
<!-- Content
================================================== -->

<!-- Container -->
<div class="container fullwidth-element">

	<!-- Google Maps -->
	<section class="google-map-container">
		<div id="googlemaps" class="google-map google-map-full"></div>
	</section>
	<!-- Google Maps / End -->

</div>
<!-- Container / End -->


<!-- Container -->
<div class="container">
<div class="four columns">

	<!-- Information -->
	<div class="widget margin-top-10">
		<div class="">

			<div>
				<p>Ariel Di Lorenzo
                    Ventas / Sales Manager</p>
                <p>Cel - Mobile. 00 54 9 11 2283 0806</p>
                <p>WhatsApp. +54 9 11 2283 0806</p>
                <p> Id Nextel: 54*136*631</p>
                <p> Skype: ariel.dilorenzo</p>
            </div>

            <div>
				<ul class="contact-informations margin-top-0">
					<li><span class="address">Saavedra 6652, Loma Hermosa</span></li>
					<li><span class="address">Buenos Aires, Argentina</span></li>
				</ul>
			</div>

			<div>
				<ul class="contact-informations second">
					<li><i class="fa fa-phone"></i> <p>00 54 11 4767 5515</p></li>
                    <li><i class="fa fa-phone"></i> <p>00 54 11 4739 9533</p></li>
					<li><i class="fa fa-envelope"></i> <p>dilorenzoariel@lomaplast.com</p></li>
					<li><i class="fa fa-globe"></i> <p>www.example.com</p></li>
				</ul>
			</div>

		</div>
	</div>

</div>

<!-- Contact Form -->
<div class="twelve columns">
	<div class="extra-padding left">
		<h3 class="headline">Envianos tu mensaje</h3><span class="line margin-bottom-25"></span><div class="clearfix"></div>

		<!-- Contact Form -->
		<section id="contact">

			<!-- Success Message -->
			<mark id="message"></mark>

			<!-- Form -->
			<form method="post" action="send-mail" name="contactform" id="contactform">
				<div class="six-columns">
				<fieldset>

					<div>
						<label>Nombre:<span>*</span></label>
						<input name="name" type="text" id="name" />
					</div>
					<div>
						<label>Apellido:<span>*</span></label>
						<input name="apellido" type="text" id="apellido" />
					</div>
					<div>
						<label>Razón Social:<span>*</span></label>
						<input name="razon" type="text" id="razon" />
					</div>
					<div>
						<label>Dirección:<span>*</span></label>
						<input name="direccion" type="text" id="direccion" />
					</div>	
					</div>	
					</fieldset>
					<fieldset>

					<div class="six-columns">			
					
					<div>
						<label>Ciudad:<span>*</span></label>
						<input name="ciudad" type="text" id="ciudad" />
					</div>
					<div>
						<label>Codigo Postal:<span>*</span></label>
						<input name="zp" type="text" id="zp" />
					</div>
					<div>
						<label>Pais:<span>*</span></label>
						<select name="pais" id="pais">
						<option value="" selected="">Seleccionar Pais</option>
							<option value="AF">Afghanistan</option>
								<option value="AL">Albania</option>
								<option value="DE">Alemania</option>
								<option value="AD">Andorra</option>
								<option value="AO">Angola</option>
								<option value="AI">Anguilla</option>
								<option value="AG">Antigua y Barbuda</option>
								<option value="AN">Antillas Holandesas</option>
								<option value="AQ">Antártida</option>
								<option value="SA">Arabia Saudí</option>
								<option value="DZ">Argelia</option>
								<option value="AR">Argentina</option>
								<option value="AM">Armenia</option>
								<option value="AW">Aruba</option>
								<option value="AU">Australia</option>
								<option value="AT">Austria</option>
								<option value="AZ">Azerbaiyán</option>
								<option value="BS">Bahamas</option>
								<option value="BH">Bahrein</option>
								<option value="BD">Bangladesh</option>
								<option value="BB">Barbados</option>
								<option value="BZ">Belice</option>
								<option value="BJ">Benin</option>
								<option value="BM">Bermudas</option>
								<option value="BY">Bielorrusia</option>
								<option value="BO">Bolivia</option>
								<option value="BQ">Bonaire, Sint Eustatius and Saba</option>
								<option value="BA">Bosnia y Herzegovina</option>
								<option value="BW">Botswana</option>
								<option value="BR">Brasil</option>
								<option value="BN">Brunei</option>
								<option value="BG">Bulgaria</option>
								<option value="BF">Burkina Faso</option>
								<option value="BI">Burundi</option>
								<option value="BT">Bután</option>
								<option value="BE">Bélgica</option>
								<option value="CV">Cabo Verde</option>
								<option value="KH">Camboya</option>
								<option value="CM">Camerún</option>
								<option value="CA">Canadá</option>
								<option value="QA">Catar</option>
								<option value="CL">Chile</option>
								<option value="CN">China</option>
								<option value="CY">Chipre</option>
								<option value="VA">Ciudad del Vaticano</option>
								<option value="CO">Colombia</option>
								<option value="KM">Comoras</option>
								<option value="CG">Congo</option>
								<option value="CD">Congo (Rep. Dem.)</option>
								<option value="KP">Corea del Norte</option>
								<option value="KR">Corea del Sur</option>
								<option value="CR">Costa Rica</option>
								<option value="CI">Costa de Marfil</option>
								<option value="HR">Croacia</option>
								<option value="CU">Cuba</option>
								<option value="CW">Curaçao</option>
								<option value="DK">Dinamarca</option>
								<option value="DM">Dominica</option>
								<option value="EC">Ecuador</option>
								<option value="EG">Egipto</option>
								<option value="SV">El Salvador</option>
								<option value="AE">Emiratos Árabes Unidos</option>
								<option value="ER">Eritrea</option>
								<option value="SI">Eslovenia</option>
								<option value="ES">España</option>
								<option value="US">Estados Unidos</option>
								<option value="EE">Estonia</option>
								<option value="ET">Etiopía</option>
								<option value="PH">Filipinas</option>
								<option value="FI">Finlandia</option>
								<option value="FJ">Fiyi</option>
								<option value="FR">Francia</option>
								<option value="GA">Gabón</option>
								<option value="GM">Gambia</option>
								<option value="GE">Georgia</option>
								<option value="GH">Ghana</option>
								<option value="GI">Gibraltar</option>
								<option value="GR">Grecia</option>
								<option value="GD">Grenada</option>
								<option value="GL">Groenlandia</option>
								<option value="GP">Guadalupe</option>
								<option value="GU">Guam</option>
								<option value="GT">Guatemala</option>
								<option value="GF">Guayana Francesa</option>
								<option value="GG">Guernsey y Alderney</option>
								<option value="GN">Guinea</option>
								<option value="GQ">Guinea Ecuatorial</option>
								<option value="GW">Guinea-Bisáu</option>
								<option value="GY">Guyana</option>
								<option value="HT">Haiti</option>
								<option value="HN">Honduras</option>
								<option value="HK">Hong Kong</option>
								<option value="HU">Hungría</option>
								<option value="IN">India</option>
								<option value="ID">Indonesia</option>
								<option value="IQ">Irak</option>
								<option value="IR">Iran</option>
								<option value="IE">Irlanda</option>
								<option value="BV">Isla Bouvet</option>
								<option value="IM">Isla de Man</option>
								<option value="CX">Isla de Navidad</option>
								<option value="NF">Isla de Norfolk</option>
								<option value="IS">Islandia</option>
								<option value="KY">Islas Caimán</option>
								<option value="CC">Islas Cocos o Islas Keeling</option>
								<option value="CK">Islas Cook</option>
								<option value="FO">Islas Faroe</option>
								<option value="GS">Islas Georgias del Sur y Sandwich del Sur</option>
								<option value="HM">Islas Heard y McDonald</option>
								<option value="FK">Islas Malvinas</option>
								<option value="MP">Islas Marianas del Norte</option>
								<option value="MH">Islas Marshall</option>
								<option value="PN">Islas Pitcairn</option>
								<option value="SB">Islas Salomón</option>
								<option value="SJ">Islas Svalbard y Jan Mayen</option>
								<option value="TK">Islas Tokelau</option>
								<option value="TC">Islas Turks y Caicos</option>
								<option value="VI">Islas Vírgenes de los Estados Unidos</option>
								<option value="VG">Islas Vírgenes del Reino Unido</option>
								<option value="UM">Islas menores de Estados Unidos</option>
								<option value="IL">Israel</option>
								<option value="IT">Italia</option>
								<option value="JM">Jamaica</option>
								<option value="JP">Japón</option>
								<option value="JE">Jersey</option>
								<option value="JO">Jordania</option>
								<option value="KZ">Kazajistán</option>
								<option value="KE">Kenia</option>
								<option value="KG">Kirguizistán</option>
								<option value="KI">Kiribati</option>
								<option value="KW">Kuwait</option>
								<option value="LA">Laos</option>
								<option value="LS">Lesotho</option>
								<option value="LV">Letonia</option>
								<option value="LR">Liberia</option>
								<option value="LY">Libia</option>
								<option value="LI">Liechtenstein</option>
								<option value="LT">Lituania</option>
								<option value="LU">Luxemburgo</option>
								<option value="LB">Líbano</option>
								<option value="MO">Macao</option>
								<option value="MK">Macedonia</option>
								<option value="MG">Madagascar</option>
								<option value="MY">Malasia</option>
								<option value="MW">Malawi</option>
								<option value="MV">Maldivas</option>
								<option value="ML">Mali</option>
								<option value="MT">Malta</option>
								<option value="MA">Marruecos</option>
								<option value="MQ">Martinica</option>
								<option value="MU">Mauricio</option>
								<option value="MR">Mauritania</option>
								<option value="YT">Mayotte</option>
								<option value="FM">Micronesia</option>
								<option value="MD">Moldavia</option>
								<option value="MN">Mongolia</option>
								<option value="ME">Montenegro</option>
								<option value="MS">Montserrat</option>
								<option value="MZ">Mozambique</option>
								<option value="MM">Myanmar</option>
								<option value="MX">México</option>
								<option value="MC">Mónaco</option>
								<option value="NA">Namibia</option>
								<option value="NR">Nauru</option>
								<option value="NP">Nepal</option>
								<option value="NI">Nicaragua</option>
								<option value="NG">Nigeria</option>
								<option value="NU">Niue</option>
								<option value="NO">Noruega</option>
								<option value="NC">Nueva Caledonia</option>
								<option value="NZ">Nueva Zelanda</option>
								<option value="NE">Níger</option>
								<option value="OM">Omán</option>
								<option value="PK">Pakistán</option>
								<option value="PW">Palau</option>
								<option value="PS">Palestina</option>
								<option value="PA">Panamá</option>
								<option value="PG">Papúa Nueva Guinea</option>
								<option value="PY">Paraguay</option>
								<option value="NL">Países Bajos</option>
								<option value="PE">Perú</option>
								<option value="PF">Polinesia Francesa</option>
								<option value="PL">Polonia</option>
								<option value="PT">Portugal</option>
								<option value="PR">Puerto Rico</option>
								<option value="GB">Reino Unido</option>
								<option value="CF">República Centroafricana</option>
								<option value="CZ">República Checa</option>
								<option value="DO">República Dominicana</option>
								<option value="SK">República Eslovaca</option>
								<option value="ZA">República de Sudáfrica</option>
								<option value="RE">Reunión</option>
								<option value="RW">Ruanda</option>
								<option value="RO">Rumania</option>
								<option value="RU">Rusia</option>
								<option value="EH">Sahara Occidental</option>
								<option value="BL">Saint Barthélemy</option>
								<option value="KN">Saint Kitts y Nevis</option>
								<option value="MF">Saint Martin</option>
								<option value="WS">Samoa</option>
								<option value="AS">Samoa Americana</option>
								<option value="SM">San Marino</option>
								<option value="PM">San Pedro y Miquelón</option>
								<option value="VC">San Vicente y Granadinas</option>
								<option value="SH">Santa Helena</option>
								<option value="LC">Santa Lucía</option>
								<option value="ST">Santo Tomé y Príncipe</option>
								<option value="SN">Senegal</option>
								<option value="RS">Serbia</option>
								<option value="SC">Seychelles</option>
								<option value="SL">Sierra Leone</option>
								<option value="SG">Singapur</option>
								<option value="SX">Sint Maarten</option>
								<option value="SY">Siria</option>
								<option value="SO">Somalia</option>
								<option value="LK">Sri Lanka</option>
								<option value="SZ">Suazilandia</option>
								<option value="SD">Sudán</option>
								<option value="SS">Sudán del Sur</option>
								<option value="SE">Suecia</option>
								<option value="CH">Suiza</option>
								<option value="SR">Surinam</option>
								<option value="TH">Tailandia</option>
								<option value="TW">Taiwán</option>
								<option value="TZ">Tanzania</option>
								<option value="TJ">Tayikistán</option>
								<option value="IO">Territorio Británico del Océano Índico</option>
								<option value="TF">Territorios Franceses del Sur</option>
								<option value="TL">Timor Oriental</option>
								<option value="TG">Togo</option>
								<option value="TO">Tonga</option>
								<option value="TT">Trinidad y Tobago</option>
								<option value="TM">Turkmenistán</option>
								<option value="TR">Turquía</option>
								<option value="TV">Tuvalu</option>
								<option value="TN">Túnez</option>
								<option value="UA">Ucrania</option>
								<option value="UG">Uganda</option>
								<option value="UY">Uruguay</option>
								<option value="UZ">Uzbekistán</option>
								<option value="VU">Vanuatu</option>
								<option value="VE">Venezuela</option>
								<option value="VN">Vietnam</option>
								<option value="WF">Wallis y Futuna</option>
								<option value="YE">Yemen</option>
								<option value="DJ">Yibuti</option>
								<option value="ZM">Zambia</option>
								<option value="ZW">Zimbabue</option>
								<option value="TD">chad</option>
						</select>
						<div>
											</div>
					</div>
						<label >Email: 	<span>*</span></label>
						<input name="email" type="email" id="email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" />
					</div>

				
				</fieldset>
					<br>
					<div>
						<label>Mensaje: <span>*</span></label>
						<textarea name="comment" cols="40" rows="3" id="comment" spellcheck="true"></textarea>
					</div>
					
				<div id="result"></div>
				<input type="submit" class="submit" id="submit" value="Enviar mensaje" />
				<div class="clearfix"></div>
			
			</form>
			</div>
				<div class="enviado"><h2>Su mensaje fue enviado, Gracias!<h2></div>
		</section>
		<!-- Contact Form / End -->

	</div>
</div>
</div>
<!-- Container / End -->

