<!-- Content
================================================== -->
<div class="twelve columns">

    <!-- Ordering -->
    <h3>CHEVROLET CLASSIC 10</h3>

</div>

<!-- Products -->
<div class="twelve columns products">

    <section class="comments">

        <ul>
            <li>
                <div class="comment-content">
                    <div class="avatar">
                        <img src="http://lomaplast.com/images/productos/001604.jpg" alt="" /></div>


                    <div class="comment-by"><strong>PARAGOLPE DELANTERO NEGRO LISO</strong>
                    </div>
                    <p>CHEVROLET CLASSIC 10
                    </p>
                    <p> COD: LHT-604 </p>
                </div>
            </li>

            <li>
                <div class="comment-content">
                    <div class="avatar">
                        <img src="http://lomaplast.com/images/productos/005904.jpg" alt="" /></div>


                    <div class="comment-by"><strong>PARAGOLPE DELANTERO NEGRO LISO</strong>
                    </div>
                    <p>CHEVROLET CLASSIC 10
                    </p>
                    <p> COD: LHT-604 </p>
                </div>
            </li>

            <li>
                <div class="comment-content">
                    <div class="avatar">
                        <img src="http://lomaplast.com/images/productos/005905.jpg" alt="" /></div>


                    <div class="comment-by"><strong>PARAGOLPE DELANTERO NEGRO LISO</strong>
                    </div>
                    <p>CHEVROLET CLASSIC 10
                    </p>
                    <p> COD: LHT-604 </p>
                </div>
            </li>

            <li>
                <div class="comment-content">
                    <div class="avatar">
                        <img src="http://lomaplast.com/images/productos/001604.jpg" alt="" /></div>


                    <div class="comment-by"><strong>PARAGOLPE DELANTERO NEGRO LISO</strong>
                    </div>
                    <p>CHEVROLET CLASSIC 10
                    </p>
                    <p> COD: LHT-604 </p>
                </div>
            </li>

        </ul>

        <div id="small-dialog" class="zoom-anim-dialog mfp-hide">
            <h3 class="headline">Add Review</h3><span class="line margin-bottom-25"></span><div class="clearfix"></div>

            <!-- Form -->
            <form id="add-comment" class="add-comment">
                <fieldset>

                    <div>
                        <label>Name:</label>
                        <input type="text" value=""/>
                    </div>

                    <div>
                        <label>Rating:</label>
                                        <span class="rate">
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                        </span>
                        <div class="clearfix"></div>
                    </div>

                    <div class="margin-top-20">
                        <label>Email: <span>*</span></label>
                        <input type="text" value=""/>
                    </div>

                    <div>
                        <label>Review: <span>*</span></label>
                        <textarea cols="40" rows="3"></textarea>
                    </div>

                </fieldset>

                <a href="#" class="button color">Add Review</a>
                <div class="clearfix"></div>

            </form>
        </div>

    </section>


</div>

</div>