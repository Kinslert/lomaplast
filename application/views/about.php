
<!-- Content
================================================== -->

<!-- Container -->

<div class="container">
	<div class="sixteen columns">	
		<h3 class="headline">LOMA HERMOSA PLAST S.A</h3><span class="line margin-bottom-30"></span><div class="clearfix"></div>
	</div>

	<!-- Who We Are? -->
	<div class="eight columns">
        <p>Somos una empresa dedicada a la inyeccion de paragolpes plásticos, para el mercado argentino y extranjero.
            Exportamos a diferentes paises y contamos con años de experiencia atendiendo siempre a las inquietudes y solicitudes de nuestros clientes lo que nos permite lograr su satisfacción siempre y liderar el mercado</p>

        <p>Contamos con una planta de 4200 metros cuadrados de superficie y maquinaria de última generación para la fabricación y comercialización de piezas de alta calidad. Capaces de aprobar cualquier tipo de exigencia y normas de calidad, para satisfacer los requerimientos de nuestros clientes, en un mercado cada vez mas exigente</p>
	</div>

	<!-- Our Skills -->
	<div class="eight columns">

	</div>

</div>
<!-- Container / End -->

<br>

    
<!-- Container -->
<div class="container">
	
	<div class="four columns eyes">
		<div class="counter-box firstclass">
			<i class="fa fa-thumbs-o-up"></i>
			<span class="counter">4,200</span>
			<p>Mts cuadrados de producción</p>
			<p><?php  ?></p>
		</div>

	</div>

	<div class="four columns eyesb">
		<div class="counter-box firstclass">
			<i class="fa fa-group"></i>
			<span class="counter">12</span>
			<p>Países donde exportamos</p>
		</div>
	
	</div>

		<div class="four columns eyesc">
		<div class="counter-box firstclass">
			<i class="fa  fa-shopping-cart"></i>
			<span class="counter">130</span>
			<p>Artículos de producción propia</p>

		</div>
	
	</div>
		
		<div id="slide-a">
    	<img src="images/slide_about/example-slide-1.jpg" alt="Photo by: Missy S Link: http://www.flickr.com/photos/listenmissy/5087404401/">
 		<img src="images/slide_about/example-slide-2.jpg" alt="Photo by: Daniel Parks Link: http://www.flickr.com/photos/parksdh/5227623068/">
		<img src="images/slide_about/example-slide-3.jpg" alt="Photo by: Mike Ranweiler Link: http://www.flickr.com/photos/27874907@N04/4833059991/">
      <img src="images/slide_about/example-slide-4.jpg" alt="Photo by: Stuart SeegerLink: http://www.flickr.com/photos/stuseeger/97577796/">
    <a href="#" class="slidesjs-previous slidesjs-navigation"><i class="fa fa-chevron-left icon-large"></i></a>
    <a href="#" class="slidesjs-next slidesjs-navigation"><i class="fa fa-chevron-right icon-large"></i></a>
    </div>

    <div id="slide-b">
    	<img src="http://lorempixel.com/586/330/" alt="Photo by: Missy S Link: http://www.flickr.com/photos/listenmissy/5087404401/">
 		<img src="images/slide_about/example-slide-2.jpg" alt="Photo by: Daniel Parks Link: http://www.flickr.com/photos/parksdh/5227623068/">
		<img src="images/slide_about/example-slide-3.jpg" alt="Photo by: Mike Ranweiler Link: http://www.flickr.com/photos/27874907@N04/4833059991/">
      <img src="images/slide_about/example-slide-4.jpg" alt="Photo by: Stuart SeegerLink: http://www.flickr.com/photos/stuseeger/97577796/">
    <a href="#" class="slidesjs-previous slidesjs-navigation"><i class="fa fa-chevron-left icon-large"></i></a>
    <a href="#" class="slidesjs-next slidesjs-navigation"><i class="fa fa-chevron-right icon-large"></i></a>
    </div>


    <div id="slide-c">
    	<img src="images/slide_about/example-slide-1.jpg" alt="Photo by: Missy S Link: http://www.flickr.com/photos/listenmissy/5087404401/">
 		<img src="images/slide_about/example-slide-2.jpg" alt="Photo by: Daniel Parks Link: http://www.flickr.com/photos/parksdh/5227623068/">
		<img src="images/slide_about/example-slide-3.jpg" alt="Photo by: Mike Ranweiler Link: http://www.flickr.com/photos/27874907@N04/4833059991/">
      <img src="images/slide_about/example-slide-4.jpg" alt="Photo by: Stuart SeegerLink: http://www.flickr.com/photos/stuseeger/97577796/">
    <a href="#" class="slidesjs-previous slidesjs-navigation"><i class="fa fa-chevron-left icon-large"></i></a>
    <a href="#" class="slidesjs-next slidesjs-navigation"><i class="fa fa-chevron-right icon-large"></i></a>
    </div>



</div>
<!-- Container / End -->

<br>