$(document).ready(function(){
	$('.enviado').fadeOut(1);
	var name = $('#name').val();
	var apellido = $('#apellido').val();
	var razon = $('#razon').val();
	var direccion = $('#direccion').val();
	var ciudad = $('#ciudad').val();
	var zp = $('#zp').val();
	var pais = $('#pais').val();
	var mail = $('#email').val();
	var comment = $('#comment').val();
	$('#contactform').validate({
		rules:{
			name:{
				required: true,
				minlength: 4 
			},
			apellido:{
				required: true,
				minlength: 4 
			},
			razon:{
				required: true,
				minlength: 4 
			},
			direccion:{
				required: true,
				minlength: 4 
			},
			ciudad:{
				required: true,
				minlength: 4 
			},
			zp:{
				required: true,
				minlength: 4 
			},
			pais:{
				required: true
			},
			mail:{
				required: false,
				mail: true
			},
			comment:{
				required: true,
				minlength: 10
			}
		},
		messages:{
			name:{
				required: 'El Nombre es obligatorio',
				minlength: 'minimo 4'
			},
			apellido:{
				required: 'El Apellido es obligatorio',
				minlength: 'minimo 4' 
			},
			razon:{
				required: 'La Razón Social obligatoria',
				minlength: 'minimo 4' 
			},
			direccion:{
				required: 'La Dirección es obligatoria',
				minlength: 'minimo 4' 
			},
			ciudad:{
				required: 'La Ciudad es obligatoria',
				minlength: 'minimo 4' 
			},
			zp:{
				required: 'el Codigo Postal es obligatorio',
			},
			pais:{
				required: 'El Pais es obligatorio',
				minlength: 'minimo 4' 
			},
			mail:{
				required: 'El Correo electronico es obligatorio',
				mail : 'Introduzca un mail valido'
			},
			comment:{
				required: 'El Mensaje es obligatorio',
				minlength: 'Minimo 10 caracteres son requeridos'
			}
		},

		submitHandler(){
			var datos = $('#contactform').serialize();
        	var url = $('#contactform').attr('action');
			$.ajax({
			url: url,
            type: 'POST',
            data: datos,
            success: function(data){
                $('#contactform').fadeOut(1500);
                $('.enviado').animate()
                $('.enviado').fadeIn(1500);
                console.log('enviado!');
            },
            error: function () {
            	console.log('Error al enviar!!');
            }
			});
	
			}
			

			});

		});