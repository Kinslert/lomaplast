  $(document).ready(function() {

    $('#magazine').turn({
              display: 'double',
              acceleration: true,
              gradients: !$.isTouch,
              elevation:50,
              when: {
                turned: function(e, page) {
                  /*console.log('Current view: ', $(this).turn('view'));*/
                }
              }
            });
            
    $("#magazine").bind("turning", function(event, page, view) {
    console.log (page);
    
    
  if (page==1) {
    $("#magazine_frame").css("margin-left","-160px"); 

    
    $("#arrow_previus").css("display","none");
    $("#arrow_next").css("display","block");    
    
    $("#arrow_previus").css("margin-left","0px");
    $("#arrow_next").css("margin-right","220px");
  }else{
    if (page == 54){
      $("#magazine_frame").css("margin-left","260px");
      
      $("#arrow_previus").css("margin-left","220px");
      $("#arrow_next").css("margin-right","0px");
      
      $("#arrow_previus").css("display","block");
      $("#arrow_next").css("display","none");
    }else{
      $("#magazine_frame").css("margin-left","50px");
      
      $("#arrow_previus").css("margin-left","0px");
      $("#arrow_next").css("margin-right","0px");
      
      $("#arrow_previus").css("display","block");
      $("#arrow_next").css("display","block");
    }
  }
});       
  });
  
  
  $(window).bind('keydown', function(e){
    
    if (e.keyCode==37)
      $('#magazine').turn('previous');
    else if (e.keyCode==39)
      $('#magazine').turn('next');
      
  });
  
  $('#arrow_previus').bind('click',function(){
      $('#magazine').turn('previous');  
  });

  $('#arrow_next').bind('click',function(){
      $('#magazine').turn('next');  
  });
